/**
 * Created by CCNE on 14/09/2016.
 */
public class Student {
    private String name,id,group,gmail;
    //Ham tao mac dinh
    public Student(){};
    //Ham tao co tham so
    public Student(String name, String id, String group, String gmail) {
        this.name = name;
        this.id = id;
        this.group = group;
        this.gmail = gmail;
    }
    //Ham tao sao chep
    public Student(Student other) {
        this.name = other.name;
        this.id = other.id;
        this.group = other.group;
        this.gmail =other.gmail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getGmail() {
        return gmail;
    }

    public void setGmail(String gmail) {
        this.gmail = gmail;
    }
    //Phuong thuc in thong tin sinh vien
    public void getInfo()
    {
        System.out.println("Thong tin sinh vien la : ");
        System.out.println("Ho va ten : "+getName()+"\nMSSV: "+getId()+"\nGroup: "+getGroup()+"\nGmail: "+getGmail());
    }
}

/**
 * Created by TUANANH on 13/10/2016.
 */
import java.util.ArrayList;

public class Manager extends Employee {
    ArrayList<Group> groups;
    ArrayList<Report> reports;
    public Manager(String name,String phone) {
        super(name,phone);
        groups = new ArrayList<Group>();
        reports = new ArrayList<Report>();
    }
    public void receiveReport(Report r)
    {
        reports.add(r);
    }
    public void reviewReport()
    {
        for (int i =0; i<reports.size();i++) {
            System.out.println("---------------A report "+(i+1)+"-----------");
            System.out.println("Send by: "+reports.get(i).sender.getFullName());
            System.out.println("Content: "+reports.get(i).getContent()+"\nTime: "+ reports.get(i).getTime());
            System.out.println("--------------------------------------------");
        }
    }
    public Boolean addMember(Employee e, Group g)
    {
        Boolean resuilt = true;
        for (Employee x : g.getMembers()) {
            if(x.getPhoneNumber().equals(e.getPhoneNumber()))
                resuilt = false;
        }
        g.addMember(e);
        return resuilt;

    }
    public Boolean removeMember(Employee e, Group g)
    {
        Boolean resuilt = false;
        for (Employee x:g.getMembers()) {
            if(x.getPhoneNumber().equals(e.getPhoneNumber())) resuilt= true;
        }
        g.removeMember(e);
        return resuilt;
    }
    public Group createGroup(String name, TeamLeader leader)
    {
        Group p = new Group(leader,name);
        groups.add(p);
        p.setManager(this);
        return p;

    }
    public void assignGroup(Manager m, Group g)
    {
        Manager a = g.getManager();
        m.groups.add(g);
        g.setManager(m);
        Group g2 = null;
        for (Group x:a.groups) {
            if(x.getName().equals(g.getName())) g2=x;
        }
        a.groups.remove(g2);
    }


}
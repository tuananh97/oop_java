/**
 * Created by TUANANH on 13/10/2016.
 */
public class Report {
    Employee sender;
    String content;
    String time;
    public Report(){}
    public Report(Employee sender, String content, String time) {
        this.sender = sender;
        this.content = content;
        this.time = time;
    }

    public Employee getSender() {
        return sender;
    }

    public void setSender(Employee sender) {
        this.sender = sender;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}

/**
 * Created by TUANANH on 13/10/2016.
 */



import java.time.Period;
import java.util.ArrayList;

public class Employee{
    private String fullName;
    private String phoneNumber;
    private ArrayList<String> tasks;

    public  Employee(String name,String phone){
        fullName=name;
        phoneNumber= phone;
        tasks = new ArrayList<String>();
    }
    public String getFullName(){
        return fullName;
    }
    public String getPhoneNumber()
    {
        return phoneNumber;
    }
    public void addTasks(String task)
    {

        tasks.add(task);
    }
    public void showTask()
    {
        for (int i =0; i<tasks.size();i++) {
            System.out.println(tasks.get(i));
        }
    }
    public void showEmployee(Employee e)
    {
        System.out.println(e.getFullName()+", "+e.getPhoneNumber());
        e.showTask();
    }
    public void sendReport(String content, Employee boss){
        System.out.println("Send report to "+boss.getFullName());
        System.out.println("Content: "+content);
        Report r1 = new Report();
        r1.setSender(this);
        r1.setContent(content);
        r1.setTime("20:00");
        if(boss instanceof TeamLeader){
            TeamLeader lead = (TeamLeader )boss;
            lead.receiveReport(r1);
        }
        if(boss instanceof Manager){
            Manager m = (Manager )boss;
            m.receiveReport(r1);
        }
    }
    public void preparePlan(){

        System.out.println("Prepare week plan");
    }
}


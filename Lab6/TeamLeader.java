import java.util.ArrayList;

/**
 * Created by TUANANH on 13/10/2016.
 */
public class TeamLeader extends Employee {
    ArrayList<Report> reports;
    public TeamLeader(String name, String phone) {
        super(name, phone);
        // TODO Auto-generated constructor stub
        reports = new ArrayList<Report>();
    }
    public void receiveReport(Report r)
    {

        reports.add(r);
    }
    public void reviewReport()
    {
        for (int i =0; i<reports.size();i++) {
            System.out.println("---------------A report "+(i+1)+"-----------");
            System.out.println("Send by: "+reports.get(i).sender.getFullName());
            System.out.println("Content: "+reports.get(i).getContent()+"\nTime: "+ reports.get(i).getTime());
            System.out.println("--------------------------------------------");
        }
    }
    public void assignTask(String task, Employee other)
    {
        other.addTasks(task);
    }

}

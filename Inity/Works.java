
import java.util.ArrayList;
public class Works extends TaskList {

	private ArrayList<TaskList> works;

	public Works() {
	
	}

	/*
	  Cac ham khoi tao
	 */
	public ArrayList<TaskList> getWorks() {
		if (works == null) works= new ArrayList<TaskList>();
		return this.works;
	}

	public void setWorks(ArrayList<TaskList> works) {
		this.works = works;
	}
	/*
	 * Tao ra 1 TaskList
	 */
	public TaskList createTaskList(String name) {
		TaskList result = new TaskList(name);
		getWorks().add(result);
		return result;
	}
	/*
	 * Hien thi(xem) list
	 */
	public void showList()
	{
		System.out.println("\t\t\t\tREMINDER");
		for(int i=0; i<works.size(); i++)
		{
			works.get(i).showTask();
		}
	}

	/*
	 *  Xoa 1 list
	 */
	public Boolean deleteTaskList(TaskList lis)
	{
		Boolean resiult = false;
		if(works.remove(lis))
			resiult = true;
		return resiult;
	}
    /*
     * Tim kiem List theo name
     */
	public void searchList(String name)
	{
		for(int i=0 ; i< works.size(); i++) {
			if(works.get(i).Name.equals(name))
				works.get(i).showTask();
		}
	}


}

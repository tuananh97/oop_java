
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
public class Task {
	protected String name;
	protected LocalDateTime timeBegin;
	protected LocalDateTime timeEnd;
	protected String place;
	protected String repeat;
	protected int iCheck;
    /*
      Ham tao ra 1 Task
     */
	public TaskList createTask(String name, String place, String repeat) {
		this.name = name;
		this.place = place;
		this.repeat = repeat;
		return null;
	}
   /*
      Cac ham getter,setter
    */
	public int getiCheck() {
		return iCheck;
	}

	public void setiCheck( int iCheck) {
		this.iCheck = iCheck;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getRepeat() {
		return repeat;
	}

	public void setRepeat(String repeat) {
		this.repeat = repeat;
	}
    /*
      Thiet lap thoi gian bat dau
     */
	public void setTimeBegin(int y,int m, int d,int h,int mi) {
		this.timeBegin = LocalDateTime.of(y, m, d, h, mi);
	}
    /*
      Thiet lap thoi gian ket thuc
     */
	public void setTimeEnd(int y,int m, int d,int h,int mi) {

		this.timeEnd = LocalDateTime.of(y, m, d, h, mi);
	}
	
	/*
	 Hien thi thong tin cac task
	 */
	public void show() {
		DateTimeFormatter f = DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm");
		System.out.println("|Cong viec: "+getName()+ "\n|Thoi gian bat dau: "+this.timeBegin.format(f)+"\n|Thoi gian ket thuc: "+ this.timeEnd.format(f)+"\n|Noi lam: "+getPlace()  +"\n|Lap lai: "+getRepeat()+"\n|Trang thai: "+ checkDone() +"\n" );
	}
    /*
     Kiem tra xem cac Task da hoan thanh chua
     */
	public String checkDone()
	{
		if(getiCheck()== 1)
			return "Doing";
		else if(getiCheck() == 2)
			return "Done";
		else return "Todo";

	}

}


import java.time.LocalDate;

public class ReminderManager {
	public static void main(String[] args) {
		/*
	      Tao ra cac TaskList
		 */
		Works tab = new Works();
		TaskList list1 = tab.createTaskList("Daily Work");
		TaskList list2 = tab.createTaskList("Excercise");
		
		/*
		 Tao cac Task 
		 */
		Task t = new Task();
		t.createTask("Shopping","Home","Every day");;
		t.setTimeBegin(2016,11,15, 4, 56);
		t.setTimeEnd(2016,11, 1, 6, 32);

		Task t1 = new Task();
		t1.createTask("BT OOP2","UET","Everyday");
		t1.setTimeBegin(2016, 12, 15, 4, 56);
		t1.setTimeEnd(2016, 12, 1, 6, 32);

		Task t2 = new Task();
		t2.createTask("BT OOP3","UET","Everyday");
		t2.setTimeBegin(2016, 1, 15, 4, 56);
		t2.setTimeEnd(2016, 3, 1, 6, 32);

		/*
		  Them cac Task vao List
		 */
		list1.addTask(t);
		list1.addTask(t1);
		list2.addTask(t2);
		
        /*
		  Hien thi cac Task thuoc cac List
		*/
		tab.showList();
		
		/*
		  In ra cac Task da hoan thanh trong 1 tuan ke tu truoc ngay 6/11/2016
		*/
        System.out.println(" Task da hoan thanh trong 1 tuan ke tu truoc ngay 6/11/2016");
		LocalDate date = LocalDate.of(2016, 11, 6);
		list1.Taskdone(date);

	}

}

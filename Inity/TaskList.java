
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;

public class TaskList {
	protected String Name;
	protected ArrayList<Task> tabWorks;
	/*
     Khoi tao 1 TaskList
	 */
	public TaskList(){}
	public TaskList(String name) {
		super();
		this.Name = name;
	}
	/*
      Cac ham getter,setter
	 */
	public String getName() {
		return Name;
	}

	public void setName(String name) {
		this.Name = name;
	}

	public ArrayList<Task> getTabWorks() {
		if (tabWorks == null) tabWorks= new ArrayList<Task>();
		return this.tabWorks;
	}

	public void setTabWorks(ArrayList<Task> tabWorks) {
		this.tabWorks = tabWorks;
	}

	/*
     Them 1 Task vao List
	 */
	public void addTask(Task t1)
	{
		getTabWorks().add(t1);
	}
	
	/*
      Hien thi cac Task thuoc mot List
	 */
	public void showTask() {
		System.out.println("\t\t" + getName());
		for( int i=0; i<tabWorks.size(); i++)
		{
			this.tabWorks.get(i).show();
		}
		System.out.print("----------------------------------------------------\n");
	}

	/*
      Xoa bo 1 task ra khoi List
	 */
	public Boolean deleteTask(Task tf) {
		Boolean resiult = false;
		if(tabWorks.remove(tf))
			resiult = true;
		return resiult;
	}

	/*
	 * Tim kiem Task theo ten trong 1 List
	 */
	public void searchTask(String name)
	{
		for(int i=0 ; i<tabWorks.size(); i++) {
			if(tabWorks.get(i).name.equals(name))
				tabWorks.get(i).show();
		}
	}

	/*
        Hien thi cac task da hoan thanh trong 1 tuan tinh tu ngay muon kiem tra
	 */
	public void Taskdone(LocalDate d)
	{

		for (Task task : tabWorks) {
			LocalDate d1 = task.timeEnd.toLocalDate();
			Period p = Period.between(d1, d);
			if(p.getDays() <=7)
				task.show();
			System.out.println();
		}

	}
}

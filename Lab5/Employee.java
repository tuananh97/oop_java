/**
 *@author TUANANH
 *@version 1 Oct 6, 2016
 */
import java.util.*;
public class Employee{
    private String fullName;
    private String phoneNumber;
    private ArrayList<String> tasks;

    public  Employee(String name,String phone){
        fullName=name;
        phoneNumber= phone;
        tasks = new ArrayList<String>();
    }
    public String getFullName(){
        return fullName;
    }
    public String getPhoneNumber()
    {
        return phoneNumber;
    }
    public void addTasks(String task)
    {

        tasks.add(task);
    }
    public void showTask()
    {
        for (int i =0; i<tasks.size();i++) {
            System.out.println(tasks.get(i));
        }
    }
    public void showEmployee(Employee e)
    {
        System.out.println(e.getFullName()+", "+e.getPhoneNumber());
        e.showTask();
    }
    public void sendReport(String content, Employee boss){
        System.out.println("Send report to "+boss.getFullName());
        System.out.println("Content: "+content);
    }
    public void preparePlan(){

        System.out.println("Prepare week plan");
    }
}


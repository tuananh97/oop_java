/**
 *@author TUANANH
 *@version 1 Oct 6, 2016
 */
public class Application {
    public static void main(String[] args){
        Manager m1 = new Manager("anh", "1111");
        Manager m2 = new Manager("ku", "111234");
        Employee e1=new Employee("E1","1111");
        Employee e2=new Employee("E2","11112");
        Employee e3 = new Employee("E3", "1123");
        TeamLeader leader1 = new TeamLeader("boss1", "1123");
        Group g1 = m1.createGroup("L60CC",leader1);
        m1.addMember(e1,g1);
        g1.showMembers();

    }

}
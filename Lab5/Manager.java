import java.util.ArrayList;
public class Manager extends Employee {
    ArrayList<Group> groups;
    public Manager(String name,String phone) {
        super(name,phone);
        groups = new ArrayList<Group>();
    }
    public Boolean addMember(Employee e, Group g)
    {
        Boolean resuilt = true;
        for (Employee x : g.getMembers()) {
            if(x.getPhoneNumber().equals(e.getPhoneNumber()))
                resuilt = false;
        }
        g.addMember(e);
        return resuilt;

    }
    public Boolean removeMember(Employee e, Group g)
    {
        Boolean resuilt = false;
        for (Employee x:g.getMembers()) {
            if(x.getPhoneNumber().equals(e.getPhoneNumber())) resuilt= true;
        }
        g.removeMember(e);
        return resuilt;
    }
    public Group createGroup(String name, TeamLeader leader)
    {
        Group p = new Group(leader,name);
        groups.add(p);
        p.setManager(this);
        return p;

    }
    public void assignGroup(Manager m,Group g)
    {
        Manager a = g.getManager();
        m.groups.add(g);
        g.setManager(m);
        Group g2 = null;
        for (Group x:a.groups) {
            if(x.getName().equals(g.getName())) g2=x;
        }
        a.groups.remove(g2);
    }


}
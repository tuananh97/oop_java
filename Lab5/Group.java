import java.util.*;

/**
 /**
 *@author TUANANH
 *@version 1 Oct 6, 2016
 */
import java.util.ArrayList;
import java.util.Iterator;
class  Group{
    private String groupName;
    private TeamLeader leader;
    Manager manager;
    private ArrayList<Employee> members;

    public Group(TeamLeader leader,String name){
        super();
        this.leader = leader;
        this.groupName=name;
        members=new ArrayList<Employee>();
    }
    public String getName(){
        return groupName;
    }
    public void setLeader(TeamLeader leader1)
    {
        this.leader = leader1;
    }
    public TeamLeader getLeader(){
        return leader;
    }
    public void showLeader()
    {
        System.out.println("Leader group 1  "+ leader.getFullName()+", "+ leader.getPhoneNumber()) ;

    }
    public void addMember(Employee m){

        members.add(m);
    }
    public ArrayList<Employee> getMembers()
    {
        return members;
    }
    public void removeMember(Employee m)
    {

        members.remove(m);
    }
    public void showMembers(){
        System.out.println("Members of "+groupName);
        Iterator<Employee> itr = members.iterator();
        while(itr.hasNext()) {
            Employee e = (Employee)itr.next();
            e.showEmployee(e);
        }
        showLeader();
    }
    public Manager getManager(){
        return manager;
    }
    public void setManager(Manager manager) {
        this.manager=manager;
    }
}

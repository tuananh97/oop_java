/**
 * Created by TUANANH on 3/10/2016.
 */
public class StudentManager {
    private Class[] classes;
    public StudentManager()  {};
    public StudentManager(Class[] classes)
    {
        this.classes = classes;
    }

    public Class[] getClasses() {
        return classes;
    }

    public void setClasses(Class[] classes) {
        this.classes = classes;
    }

    public Boolean createAClass(Class cl){
        Boolean resuilt = false;
        for(int i=0 ; i<classes.length; i++)
        {
            if((classes[i] != null)&&(classes[i].getName().equalsIgnoreCase(cl.getName())))
                return false;
        }
        for ( int i =0 ; i<classes.length; i++)
        {
            if(classes[i] == null)
            {
                classes[i] = cl;
                resuilt = true;
                break;
            }
        }
        return resuilt;
    }
    public Boolean removeAClass(Class cl)
    {
        Boolean resuilt = false;

        for(int i = 0; i<classes.length; i++)
        {
            if((classes[i] != null) && (classes[i] == cl))
            {
                if(classes[i].getMembers() [0] != null)
                    return  false;
                else
                    for(int j = i; j< classes.length -1; j++)
                    {
                        classes[j] = classes[j+1];
                    }
                    resuilt = true;
                break;
            }
        }
        return resuilt;
    }

    public Boolean updateAClass(Class cl, String rename)
    {
        Boolean resuilt = false;

        for(int i =0 ; i < classes.length; i++)
        {
            if((classes[i] != null) && (classes[i] == cl))
            {
                if(classes[i].getName().equalsIgnoreCase(rename))
                    return false;
                else{
                    classes[i].setName(rename);
                    resuilt = true;
                    break;
                }
            }
        }
        return resuilt;
    }

    public Boolean addStdentToClass(Student st, Class cl)
    {
        Boolean resuilt = false;

        for(int i = 0; i < classes.length; i++)
        {
            if(classes[i] != null)
                for(int j = 0; j < classes[i].getMembers().length; j++) {
                    if ((classes[i].getMembers()[j] != null) && (classes[i].getMembers()[j].getId().equalsIgnoreCase(st.getId()))) {
                        return false;
                    }
                }
        }

        for(int i = 0; i< classes.length; i++){
             if(classes[i] != null && classes[i] == cl){
                 classes[i].add(st);
                 resuilt = true;
                 break;
             }
        }
       return resuilt;
    }

    public void removeStudent(Student st)
    {
        for (int i = 0; i < classes.length; i++){
            if(classes[i] != null)
                classes[i].remove(st);
        }
    }

    public Boolean moveStudent(Student st, Class cl) {
        Boolean resuilt = false;
        removeStudent(st);
        if (addStdentToClass(st, cl))
            resuilt = true;

        return resuilt;
    }

    public static void main(String[] args) {

        StudentManager UET = new StudentManager();
        Class[] cl = new Class[5];
        UET.setClasses(cl);

        cl[0] = new Class(" K60CD ");
        cl[1] = new Class(" K60CC ");
        Student s1 = new Student("1111","Minh");
        Student s2 = new Student("1112","Xuan");
        Student s3 = new Student("1113","Ngo");
        Student s4 = new Student("1114","Tuyen");

        UET.addStdentToClass(s1, cl[0]);
        UET.addStdentToClass(s3, cl[0]);
        UET.addStdentToClass(s2, cl[1]);
        UET.addStdentToClass(s4, cl[1]);
        cl[0].PrintfStudent();
        cl[1].PrintfStudent();

    }

}

/**
 * Created by TUANANH on 3/10/2016.
 */
public class Class {
    private String name;
    private  Student[] members;
    //Constructor Default
    public Class(){}
    //Constructor
    public Class(String name) {
        this.name = name;
        this.members = new Student[30];
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Student[] getMembers() {
        return members;
    }

    public void setMembers(Student[] members) {
        this.members = members;
    }
    // Print imformation Student
    public void PrintfStudent()
    {
        for(int i=0 ; i<members.length ;i++)
        {
            if(members[i] != null)
            {
                System.out.println(members[i].getId()+"   "+members[i].getName());
            }
        }

    }
    // Add student to class
    public void add(Student st)
    {
        for( int i=0 ; i<members.length; i++)
        {
            if(members[i] == null)
            {
                members[i] = st;
                break;
            }
        }
    }
    // Remove student from class
    public void remove(Student st)
    {
        for(int i=0 ; i<members.length; i++)
        {
            if((members[i] != null)&&(members[i].getId().equalsIgnoreCase(st.getId())))
            {
                for( int j= i; j< members.length -1; j++)
                {
                    members[j] = members[i];
                }
                break;
            }
        }
    }
}

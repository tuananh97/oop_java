package Lab3;

public class Folder {
	private String name;
	private Folder[] children;
	
	public Folder() {
		name = "New Folder";
		children = new Folder[10];
	}
	public Folder(String name) {
		this.name = name;
		children = new Folder[10];
	}
	public Folder[] getChildren() {
		return children;
	}
	public String getName() {
		return name;
	}
	public void setChildren(Folder[] children) {
		this.children = children;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void Show() {
		for (int i = 0; i < children.length; i++) {
			System.out.println((i+1)+"."+children[i].name);
		}
	}
	
}

package Lab3;

public class FileExplorer {
	private Folder root;
	private Folder currentFolder;

	public FileExplorer() {
		root = new Folder();
		currentFolder = root;
	}

	public FileExplorer(Folder root) {
		this.root = root;
		currentFolder = this.root;
	}

	public Folder getCurrentFolder() {
		return currentFolder;
	}

	public Folder getRoot() {
		return root;
	}

	public void setCurrentFolder(Folder currentFolder) {
		this.currentFolder = currentFolder;
	}

	public void setRoot(Folder root) {
		this.root = root;
	}
    public void explorer()
    {
    	Folder[] children = currentFolder.getChildren();
    	for(int i=0; i<children.length; i++)
    	{
    		System.out.println((i+1)+"."+children[i].getName());
    	}
    }
    public Folder selectFolder(Integer folderIndex)
    {
    	Folder result = null;
    	Folder[] children = currentFolder.getChildren();
    	if((-1 < folderIndex) && (folderIndex <children.length))
    		result = children[folderIndex];
    	return result;
    }
    public void enter(Folder folder)
    {
    	Folder[] children = currentFolder.getChildren();
    	for(int i=0 ; i< children.length ; i++)
    	{
    		if(children[i] == folder){
    			currentFolder = folder;
    			break;
    		}
    	}
    }
    public Folder searchFolderByName(String folderName){
    	Folder result = null;
    	Folder[] children = currentFolder.getChildren();
    	 for(int i=0 ; i<children.length ; i++)
    	 {
    		 if(children[i].getName() == folderName){
    			 result = children[i];
    			 break;
    		 }
    	 }
    	 return result;
    }
	public Boolean addANewFolder(Folder f) {
		Boolean result = false;
		Folder[] children  = currentFolder.getChildren();
		for (Folder folder : children) {
			if(f.getName().equals(folder.getName()))
				return false;
		}
		for (int i = 0; i < children.length; i++) {
			if(children[i] == null){
				children[i] = f;
				result = true;
				break;
			}
		}
		return result;
	}
	public Boolean DeleteAFolder(Folder f) {
		Boolean result = false;
		Folder[] children  = currentFolder.getChildren();
		for (int i = 0; i < children.length; i++) {
			if(children[i] == f){
				for(int j =i ; j<children.length -1; j++)
				children[j] = children[j+1];
				result = true;
				break;
			}
		}
		return result;
	}
	public Boolean RenameAFolder(Folder f, String altName) {
		Boolean result = false;
		Folder[] children  = currentFolder.getChildren();
		for (int i =0 ; i<children.length ; i++) {
			if(children[i] == f){
				if((children[i].getName()).equalsIgnoreCase(altName)){
                   children[i].setName(altName);
                   result = true;
                   break;
				}
				else
					return false;
			}
        }
		return result;
	}
	public Boolean copyAFolder(Folder folder1, Folder folder2 ) {
		Boolean resuilt = false;
		Folder[] children1 = currentFolder.getChildren();
		for (int i = 0; i < children1.length; i++) {
			if (children1[i] == folder1) {
				Folder[] children2 = folder2.getChildren();
				for (int j = 0; j < children2.length; j++) {
					if ((folder1.getName()).equalsIgnoreCase(children2[j].getName())) 
						return false;
				}
				for (int j = 0; j < children2.length; j++) {
					if (children2[j] == null) {
					children2[j] = folder1;
					resuilt = true;
					break;
					}
				}
			}
		}
		
		return resuilt;
	}
}
